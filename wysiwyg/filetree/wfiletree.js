(function ($) {

Drupal.wysiwyg.plugins['filetree'] = {

  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    return ($(node).is('div.wysiwyg-filetree'));
  },

  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    if (data.format == 'html') {
      // Prevent duplicating a filetree.
      if ($(data.node).is('div.wysiwyg-filetree') || $(data.node).is('.wysiwyg-filetree-sub')) {
        return;
      }
      var content = this._getPlaceholder(settings);
    }
    else {
      // Prevent duplicating a filetree.
      // @todo data.content is the selection only; needs access to complete content.
      if (data.content.match(/\[filetree\]/)) {
        return;
      }
      var content = '[filetree]';
    }
    if (typeof content != 'undefined') {
      window.open('?q=admin/config/media/filetree/wysiwyg-dialog', 'filetree-wysiwy-dialog', 'modal=yes,height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
      alert('settings');
      Drupal.wysiwyg.instances[instanceId].insert(content);
    }
  },

  /**
   * Replace all [filetree] tags with images.
   */
  attach: function(content, settings, instanceId) {
    content = content.replace(/\[filetree\]/g, this._getPlaceholder(settings));
    return content;
  },

  /**
   * Replace images with <!--filetree--> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    // #404532: document.createComment() required or IE will strip the comment.
    // #474908: IE 8 filetrees when using jQuery methods to replace the elements.
    // @todo Add a generic implementation for all Drupal plugins for this.
    $.each($('div.wysiwyg-filetree', $content), function (i, elem) {
      $(elem).replaceWith('[filetree]');
//      elem.parentNode.insertBefore(document.createTextNode('[filetree]'), elem);
//      elem.parentNode.insertBefore('[filetree]', elem);
//      elem.parentNode.removeChild(elem);
    });
    return $content.html();
  },

  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function (settings) {
    return '<div alt="[filetree]" title="[filetree]" class="wysiwyg-filetree mceNonEditable drupal-content">' +
      '<img src="' + settings.path + '/images/spacer.gif" class="wysiwyg-filetree-sub"/>' +
      '<p class="wysiwyg-filetree-sub">I\'m a filetree</p></div>';
  }
};

})(jQuery);
