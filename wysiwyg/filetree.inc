<?php

/**
 * @file
 * Wysiwyg API integration on behalf of Node module.
 */

/**
 * Define a Wysiwyg plugin.
 *
 * Supposed to be used for "Drupal plugins" (cross-editor plugins) only.
 *
 * @see hook_wysiwyg_plugin()
 *
 * Each plugin file in the specified plugin directory of a module needs to
 * define meta information about the particular plugin provided.
 * The plugin's hook implementation function name is built out of the following:
 * - 'hook': The name of the module providing the plugin.
 * - 'INCLUDE': The basename of the file containing the plugin definition.
 * - 'plugin': Static.
 *
 * For example, if your module's name is 'mymodule' and
 * mymodule_wysiwyg_include_directory() returned 'plugins' as plugin directory,
 * and this directory contains an "awesome" plugin file named 'awesome.inc', i.e.
 *   sites/all/modules/mymodule/plugins/awesome.inc
 * then the corresponding plugin hook function name is:
 *   mymodule_awesome_plugin()
 *
 * @see hook_wysiwyg_include_directory()
 *
 * @return
 *   Meta information about the buttons provided by this plugin.
 */
function filetree_filetree_plugin() {
  $plugins['filetree'] = array(
    // The plugin's title; defaulting to its internal name ('awesome').
    'title' => t('Filetree plugin'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/filetree',
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
//    'icon path' => 'path to icon',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'filetree.gif',
    // The button title to display on hover.
    'icon title' => t('Insert Filetree'),
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
//    'js path' => drupal_get_path('module', 'mymodule') . '/awesomeness',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'wfiletree.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
//    'css path' => drupal_get_path('module', 'mymodule') . '/awesomeness',
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'wfiletree.css',
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
    ),
    // TinyMCE-specific: Additional HTML elements to allow in the markup.
//    'extended_valid_elements' => array(
//      'tag1[attribute1|attribute2]',
//      'tag2[attribute3|attribute4]',
//    ),
  );
  return $plugins;
}
