<?php

/**
 * Implements filetree_cache_ajax_recache().
 */

function filetree_cache_ajax_recache() {
  $params = drupal_get_query_parameters($_POST);
  $cid = 'field:node:' . $params['id'];

  foreach ($params['hash'] as $hash) {
    $h = substr($hash, 0, 32);
    $dir = base64_decode(substr($hash, 32));

    db_merge('filetree_cache')
      ->key(array('dir' => $dir))
      ->fields(array('hash' => $h))
      ->execute();
  }

  $num = db_delete('cache_field')->condition('cid', $cid)->execute();

  drupal_json_output(array('id' => $cid, 'deleted' => $num));
  exit();
}
