<?php

/**
 * Implements filetree_cache_ajax_hash().
 */

function filetree_cache_ajax_rehash() {
  $params = drupal_get_query_parameters($_POST);
  $dir = base64_decode($params['dir']);
  $hash = array();
  foreach (file_scan_directory(drupal_realpath(file_build_uri($dir)), '/.*/') as $file) {
    $hash[] = $file->uri;  //uri
    $hash[] = filemtime($file->uri); //modification time
  }
  $hash = md5('.empty' . implode('', $hash));
  $h = db_select('filetree_cache', 'f')->fields('f', array('hash'))->condition('dir', $dir)->execute()->fetchAssoc();
  $h = isset($h['hash']) ? $h['hash'] : FALSE;

  $cache = db_select('cache_field', 'c')->fields('c', array('cid'))->condition('cid', 'field:node:' . $params['id'])->execute()->fetchAssoc();
  $cache = isset($cache['cid']) ? TRUE : FALSE;

  drupal_json_output(array('hash' => $hash, 'oldhash' => $h, 'cid' =>'field:node:' . $params['id'], 'cache' => $cache));
  exit();
}
