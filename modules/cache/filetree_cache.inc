<?php

function filetree_cache_admin_form($form, &$form_state) {
  global $base_url;
  $table = '<table id="filetree-cache-admin"><thead><tr><th>' . t('Content') . '</th><th>' . t('Directories') .
    '</th><th>' . t('Hash') . '</th><th>' . t('Rehash') . '<div class="filetree-cache-rehash-all">&nbsp;</div></th><th>' .
    t('Recache') . '<div class="filetree-cache-recache-all">&nbsp;</div></th></tr>' .
    '</thead><tbody>';

  $res = db_select('cache_field', 'f')->fields('f', array('cid', 'data'))
    ->condition('serialized', 1)
    ->condition('expire', 0)
    ->condition('data', '%' . db_like('[filetree') . '%', 'LIKE')
    ->execute();
  foreach ($res as $r) {
    if (substr($r->cid, 0, 11) != 'field:node:') continue;
    $nid = substr($r->cid, 11);
    $dir = $hash = array();
    $sid = 0;
    $title = db_select('node', 'n')->fields('n', array('title'))->condition('nid', $nid)->execute()->fetchAssoc();
    foreach (unserialize($r->data) as $field) {
      foreach ($field as $lang) {
        foreach ($lang as $elem) {
          if (isset($elem['value']) and ($v = $elem['value']) != '') {
            if (!preg_match_all('/(?:<p>)?\[filetree\s*(.*?)\](?:<\/p>)?/s', $v, $matches))
              continue;
            foreach ($matches[1] as $key => $passed_params) {
              // Parse the dir parameter.
              preg_match_all('/\b(dir)=(?:\"|&quot;)(.*?)(?:\"|&quot;)/', $passed_params, $matches2[$key]);
              foreach ($matches2[$key][1] as $param_key => $param_name) {
                $d = $matches2[$key][2][$param_key];
                $h = db_select('filetree_cache', 'f')->fields('f', array('hash'))->condition('dir', $d)->execute()->fetchAssoc();
                $h = isset($h['hash']) ? $h['hash'] : '';
                $dir[] = '<div title="' . drupal_realpath(file_build_uri($d)) . '">' . $d . '</div>';
                $d = base64_encode($d);
                $hash[] = '<div rel="' . $d . '" id="filetree-cache-h-' . $nid . '-' . $sid . 
                  '" class="filetree-cache-hash"><span rel="' . $h . '">--------------------------------</span></div>';
                $sid++;
              }
            }
          }
        }
      }
    }
    $table .= '<tr id="filetree-cache-tr-' . $nid . '" class="filetree-cache-hash-tr" rel="' . $nid . '"><td>' . 
      l($title['title'] , 'node/' . $nid . '/edit', array('attributes' => array('title' => 'node/' . $nid, 'target' => '_blank'))) .
      '</td><td>' . implode('', $dir) . '</td><td>' . implode('', $hash) . 
      '</td><td><div rel="' . $nid . '" class="filetree-cache-rehash" title="' . t('Generate hash') . '">&nbsp;</div>' .
      '</td><td><div rel="' . $nid . '" class="filetree-cache-recache" title="' . t('Recreate cache data') . '">&nbsp;</div>' .
      '</td></tr>';
  }
  $table .= '</tbody></table>';
  $form['filetree-cache'] = array(
    '#markup' => $table,
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'filetree_cache') . '/filetree_cache.css',
  );
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'filetree_cache') . '/filetree_cache.js',
  );
  $form['#attached']['js'][] = array(
    'data' => array('filetree_cache' => array(
      'base_url' => $base_url . '/?q=node/',
      'hash_url' => $base_url . '/?q='. 'admin/config/media/filetree/cache-rehash',
      'recache_url' => $base_url . '/?q='. 'admin/config/media/filetree/cache-recache',
    )),
    'type' => 'setting'
  );
  return $form;
}

function _filetree_cache_hash($dir) {
  $dir = file_build_uri($dir);
  $str = '';
  foreach (file_scan_directory($dir, '/./') as $k => $v) {
    $str .= filemtime($k);
  }
  return md5($str);
}
