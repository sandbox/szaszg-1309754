(function ($) {

Drupal.behaviors.filetree_cache = {
  attach: function(context, settings) {
    $('body').once('filetree_cache', function () {
      // start calculate hash.

      $('.filetree-cache-rehash-all').click(function(e) {
        if (Drupal.behaviors.filetree_cache.queue.running) return;

        Drupal.behaviors.filetree_cache.queue.running = true;
        $('.filetree-cache-rehash-all').addClass('hash-processing');

        $('tr.filetree-cache-hash-tr').each(function() {
          $(this).addClass('hash-processing');
          $(this).find('.filetree-cache-hash').each(function() {
            Drupal.behaviors.filetree_cache.queue.ids.unshift($(this).attr('id'));
            $(this).find(' > span').text('--------------------------------');
          });
        });
        Drupal.behaviors.filetree_cache.hash();
      });

      $('.filetree-cache-rehash').click(function(e) {
        if (Drupal.behaviors.filetree_cache.queue.running) return;

        Drupal.behaviors.filetree_cache.queue.running = true;

        var tr = $(this).parent().parent();
        $(tr).addClass('hash-processing');
        $(tr).find('.filetree-cache-hash').each(function() {
          Drupal.behaviors.filetree_cache.queue.ids.unshift($(this).attr('id'));
          $(this).find(' > span').text('--------------------------------');
        });
        Drupal.behaviors.filetree_cache.hash();
      });

      //recreate cache data
      $('.filetree-cache-recache').click(function(e) {
        if (Drupal.behaviors.filetree_cache.queue.running) return;

        Drupal.behaviors.filetree_cache.queue.running = true;

        var tr = $(this).parent().parent();
        $(tr).addClass('cache-processing');
        Drupal.behaviors.filetree_cache.queue.ids.unshift($(this).attr('rel'));
        Drupal.behaviors.filetree_cache.cache();
      });

      $('.filetree-cache-recache-all').click(function(e) {
        if (Drupal.behaviors.filetree_cache.queue.running) return;

        Drupal.behaviors.filetree_cache.queue.running = true;

        $('.filetree-cache-recache').each(function() {
          $(this).parent().parent().addClass('cache-processing');
          Drupal.behaviors.filetree_cache.queue.ids.unshift($(this).attr('rel'));
        });
        Drupal.behaviors.filetree_cache.cache();
      });
    });
  }
};

Drupal.behaviors.filetree_cache.queue = {'running': false, 'ids': []};

Drupal.behaviors.filetree_cache.hash = function() {
//  alert('div ' + Drupal.behaviors.filetree_cache.queue.ids.join('; '));
  var id = Drupal.behaviors.filetree_cache.queue.ids.pop();
  var div = $('div#'+id);
  var dir = $(div).attr('rel');
  var tr = $(div).parent().parent();
  var btn = $(tr).find('div.filetree-cache-rehash');
  $(btn).addClass('hash-processing');
  var tr = $(div).parent().parent();

  var got_hash = function (data, status) {
//    alert(data.hash);
    var span = $(div).find('span');
    span.text(data.hash);
    if (data.oldhash === false || data.hash != data.oldhash)
      span.addClass('out-of-sync');
    else
      span.removeClass('out-of-sync');

    span.text(data.hash);
    $(btn).removeClass('hash-processing');
    if (Drupal.behaviors.filetree_cache.queue.ids.length == 0) {
      Drupal.behaviors.filetree_cache.queue.running = false;
      $('.filetree-cache-hash-tr').removeClass('hash-processing');
      $('.filetree-cache-rehash-all').removeClass('hash-processing');
    } else {
      Drupal.behaviors.filetree_cache.hash();
    }
  };

  var error_callback = function () {
    alert('Error generating hash.');
    Drupal.behaviors.filetree_cache.queue.ids = [];
    Drupal.behaviors.filetree_cache.queue.running = false;
    $('.filetree-cache-hash-tr').removeClass('hash-processing');
    $('.filetree-cache-rehash-all').removeClass('hash-processing');
    $(btn).removeClass('hash-processing');
  };

  $.ajax({
    url: Drupal.settings.filetree_cache.hash_url,
    type: 'POST',
    dataType: 'json',
    data: {'dir': dir, 'id': $(tr).attr('rel')},
    error: error_callback,
    success: got_hash
  });
};

Drupal.behaviors.filetree_cache.cache = function() {
  var hash = [];
  var nid = Drupal.behaviors.filetree_cache.queue.ids.pop();
  var tr = $('tr#filetree-cache-tr-'+nid);
  var btn = $(tr).find('div.filetree-cache-recache');
  $(btn).addClass('cache-processing');

  var got_html = function (data, status) {
    $(btn).removeClass('cache-processing');
    $(tr).find('div.filetree-cache-hash span.out-of-sync').removeClass('out-of-sync');
    if (Drupal.behaviors.filetree_cache.queue.ids.length == 0) {
      Drupal.behaviors.filetree_cache.queue.running = false;
      $('.filetree-cache-hash-tr').removeClass('cache-processing');
      $('.filetree-cache-recache-all').removeClass('cache-processing');
    } else {
      Drupal.behaviors.filetree_cache.cache();
    }
  };

  var error_callback = function () {
    alert('Error regenerating cache.');
    Drupal.behaviors.filetree_cache.queue.ids = [];
    Drupal.behaviors.filetree_cache.queue.running = false;
    $('.filetree-cache-hash-tr').removeClass('cache-processing');
    $('.filetree-cache-recache-all').removeClass('cache-processing');
    $(btn).removeClass('cache-processing');
  };

  var get = function() {
    $.ajax({
      url: Drupal.settings.filetree_cache.base_url + nid,
      type: 'GET',
      dataType: 'html',
      error: function() {alert('Error opening node.');},
      success: got_html
    });
  };

  $(tr).find('.filetree-cache-hash').each(function() {
    hash.push($(this).find(' > span').text() + $(this).attr('rel'));
  });
  $.ajax({
    url: Drupal.settings.filetree_cache.recache_url,
    type: 'POST',
    dataType: 'json',
    data: {'id': nid, 'hash': hash},
    error: function() {alert('Error removing data from cache.');},
    success: get
  });
};

})(jQuery);