<?php

function filetree_wysiwyg_dialog($form, &$form_state) {

  $form['directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory' ),
    '#default_value' => '',
  );

  return $form;
}
